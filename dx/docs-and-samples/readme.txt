ENVIRONMENT:
export DJANGO_SETTINGS_MODULE=mysite.settings

DAPHNE:
daphne -p 8001 mysite.asgi:application


UVICORN:
gunicorn mysite.asgi:application -k uvicorn.workers.UvicornWorker

COLLECT STATIC:
python3 manage.py collectstatic --settings=mysite.settings


ws://127.0.0.1:8000/ws/chat/lobby/
{"message":"NIFFLE WIFFLE"}
