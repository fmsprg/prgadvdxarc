#0.. STUFF TO MAKE A PYTHON SHELL THINK IT'S A DJANGO SHELL
import os, django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")
django.setup()

#and see also: https://docs.djangoproject.com/en/dev/howto/custom-management-commands/


#YOU NEED TO RUN THESE THINGS FROM INSIDE THE SHELL
#python3 manage.py shell

# 1. SEND Example
from asgiref.sync import async_to_sync
import channels.layers
channel_layer = channels.layers.get_channel_layer()
async_to_sync(channel_layer.group_add)('chat_%s' % 'test','RedisChannelLayer')#<< Register the group and sub-group
async_to_sync(channel_layer.group_send)('chat_%s' % 'test',  { 'type':  'chat_message',  'message': 'good morning world'  })#<< On sending, need to address the subgroup
#What's this 'chat_message'? The function to call on the Recieving class!--^


# 2. RECEIVE Example
# channel_layer.receive accepts the name of the Channel Layer, and that's all.
from asgiref.sync import async_to_sync
import channels.layers
channel_layer = channels.layers.get_channel_layer()
async_to_sync(channel_layer.group_add)('chat_%s' % 'test','RedisChannelLayer')#<< Register the group and sub-group

while True:
    async_to_sync(channel_layer.receive)('RedisChannelLayer')#<< On receiving, do not try to address the group and sub-group

