# mysite/urls.py
from django.conf.urls import include
from django.urls import path
from django.contrib import admin

from core import views


#REMOVE THIS in Production and serve static using nginx. (and see the last line in this file)
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('chat/', include('chat.urls')),
    path('admin/', admin.site.urls),

    path('polls/', include('polls.urls')),
    path('api/core/', include('core.urls')),
    
    path('ui/', include('uibasic.urls')),
    path('', include('book.urls')),
] 

#REMOVE THIS in Production and serve static using nginx.

urlpatterns += staticfiles_urlpatterns()
print(staticfiles_urlpatterns())
