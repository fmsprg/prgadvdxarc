from django.shortcuts import render
from django.http import HttpResponse
import logging
import colorlog


def index(request):

	logging.basicConfig(level=logging.DEBUG) 

	fmt = "{log_color}{levelname} {name}: {message}"
	colorlog.basicConfig(level=logging.DEBUG, style="{", format=fmt, stream=None)

	log = logging.getLogger()

	
	log.debug("Log message goes here.")
	log.info("Log message goes here.")
	log.warning("Log message goes here.")
	log.error("Log message goes here.")
	log.critical("Log message goes here.")	

	return HttpResponse("Hello, world. You're at the polls index.  Again.")
