from django.apps import AppConfig


class UibasicConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'uibasic'
