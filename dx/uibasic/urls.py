from django.urls import path
from . import views
from dx.settings import DEBUG, STATIC_URL, STATIC_ROOT, MEDIA_URL, MEDIA_ROOT
from django.conf.urls.static import static

urlpatterns = [
    #path('', views.index, name = 'index'),
    path('mainview/', views.mainview, name = 'mainview'),
    path('a/', views.view_a, name = 'view_a'),
    path('b/', views.view_b, name = 'view_b'),
    path('c/', views.view_c, name = 'view_c'),

    path('postone/', views.mainviewPostUpdate01, name = 'mainviewPostUpdate01'),

    path('dataone/', views.dataone, name = 'dataone'),
    #path('formsjs/', views.formsjs, name = 'formsjs'),
]

#DataFlair
if DEBUG:
    urlpatterns += static(STATIC_URL, document_root = STATIC_ROOT)
    urlpatterns += static(MEDIA_URL, document_root = MEDIA_ROOT)
